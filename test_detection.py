from detect import PixelSizeCalculator
from exceptions import InvalidTipCountException
import unittest
from datetime import datetime
import os
import cv2
import glob

#Helpful functions
def read_true_distances(distfile):
    truedists = {}
    with open(distfile) as file:
        lines = file.readlines()
        for line in lines:
            words = line.split(',')
            truedists[words[0]] = float(words[1])
    return truedists

class DetectionTests(unittest.TestCase):
    def setUp(self):
        self.psc = PixelSizeCalculator()

    # Pixel size from graticule tests
    def test_pixel_size_from_graticule(self):
        now = datetime.now()
        true_pixel_sizes = read_true_distances('testvalues.txt')
        rejected_count = 0
        img_paths = [f for f in glob.glob('images/250x/*.*') if 'sl' in f or 'xl' in f]
        outlines = ['Test log ', now.strftime('%d/%m/%Y at %H:%M:%S'), '\n']
        outlines += '\nBegin pixel size tests:\n'
        print('\nBegin pixel size tests:')
        for img_path in img_paths:
            true_pixel_size = true_pixel_sizes[os.path.basename(img_path)]
            img = cv2.imread(img_path)
            try:
                mu, ci, _ = self.psc.calculate_pixel_size_from_graticule(img)
                diff = mu - true_pixel_size
                line = 'Test {} w/ true size: {} | {} +/- {} diff: {:.1%} ci/mu: {:.1%}\n'.format(
                    os.path.basename(img_path),
                    round(true_pixel_size, 3),
                    round(mu, 3),
                    round(ci, 3),
                    diff / true_pixel_size,
                    ci / mu
                )
                outlines += line
                print(line)
                # Assert mu is within 10% of true value
                assert abs(diff) < 0.1 * true_pixel_size
                # Assert CI is no more than 10% of mu
                assert ci < 0.1 * mu
            except InvalidTipCountException:
                line = 'Rejected {}\n'.format(os.path.basename(img_path))
                outlines += line
                print(line)
                rejected_count += 1
        # Assert we didn't reject more than 20% of our inputs
        print('Processed {} images\n'.format(len(img_paths)))
        print('{:.1%}'.format(rejected_count / len(img_paths)))
        with open('logs.txt', 'w') as logfile:
            logfile.writelines(outlines)
        assert rejected_count < 0.3 * len(img_paths)


if __name__=='__main__':
    unittest.main()