const s1 = document.getElementById('setup-section');
const s2 = document.getElementById('main-section');

const q1 = document.getElementById('type-question');
const q2 = document.getElementById('distance-question');

const scaleField = document.getElementById('input-scale');
const dropArea = document.getElementById('drop-area');
const uploadForm = document.getElementById('upload-form');
const loading = document.getElementById('loading');
const settings = document.getElementById('special-settings');
const settingToggle = document.getElementById('S-button');

let scale = 10;
let processing = false;
var showSettings = false;

// sleep time expects milliseconds
function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
  }

function fadeTransition(from, to) {
    from.classList.add('fade-out');
    sleep(500).then(() => {
        from.classList.add('hide');
        to.classList.remove('hide');
        to.classList.add('show-flex', 'fade-in');
    });
}

document.getElementById('btn-lines').onclick = function(e) {
    fadeTransition(q1, q2);
}

document.getElementById('btn-dists').onclick = function(e) {
    if (scaleField.checkValidity()) {
        scale = scaleField.value;
        fadeTransition(s1, s2);
    } else {
        alert("Please enter a valid distance in micron!");
    }
}

document.getElementById('S-button').onclick = function(e) {
    if (showSettings) {
        settings.classList.add('hide');
        settingToggle.innerHTML = "show"
    } else {
        settings.classList.remove('hide');
        settingToggle.innerHTML = "hide"
    }
    showSettings = !showSettings
}

;
['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
    dropArea.addEventListener(eventName, preventDefaults, false);
})

function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
}

;
['dragenter', 'dragover'].forEach(eventName => {
    dropArea.addEventListener(eventName, highlight, false);
})

;
['dragleave', 'drop'].forEach(eventName => {
    dropArea.addEventListener(eventName, unhighlight, false);
})

function highlight(e) {
    if (!processing) {
        dropArea.classList.add('highlight');
    }
}

function unhighlight(e) {
    if (!processing) {
        dropArea.classList.remove('highlight');
    }
}

dropArea.addEventListener('drop', handleDrop, false);

function handleDrop(e) {
    let dt = e.dataTransfer;
    let files = dt.files;
    handleFiles(files);
}

function handleFiles(files) {
    if (!processing) {
        processing = true;
        // Show a loading thing
        uploadForm.classList.add('hide');
        loading.classList.remove('hide');
        // Process the files
        asyncForEach([...files], uploadFile).then(
            () => {
                loading.classList.add('hide');
                uploadForm.classList.remove('hide');
                processing = false;
            }
        );
    }
}

async function uploadFile(file) {
    let formData = new FormData();
    formData.append('file', file);
    formData.append('scale', scale);
    formData.append('minimum_contour_length', document.getElementById('S-minimum_contour_length').value);
    formData.append('minimum_ellipse_aspect_ratio', document.getElementById('S-minimum_ellipse_aspect_ratio').value);
    formData.append('upper_reject_ratio', document.getElementById('S-upper_reject_ratio').value);
    formData.append('lower_reject_ratio', document.getElementById('S-lower_reject_ratio').value);
    formData.append('confidence_level', document.getElementById('S-confidence_level').value);
    formData.append('max_tip_count', document.getElementById('S-max_tip_count').value);
    formData.append('min_tip_count', document.getElementById('S-min_tip_count').value);
    formData.append('canny_kernel_size', document.getElementById('S-canny_kernel_size').value);
    formData.append('distances_to_sample', document.getElementById('S-distances_to_sample').value);
    formData.append('canny_low_threshold', document.getElementById('S-canny_low_threshold').value);
    formData.append('canny_high_threshold', document.getElementById('S-canny_high_threshold').value);

    /* Send to server */
    await fetch('process', {
            method: 'POST',
            body: formData
        })
        .then((e) => {
            e.text().then(
                t => {
                    if (t == 'invalid format') {
                        alert('Invalid format - ' + file.name);
                        processing = false;
                    } else if (t == 'invalid file') {
                        alert('Invalid file - ' + file.name);
                        processing = false;
                    } else if (t == 'no selected file') {
                        alert('No selected file!');
                        processing = false;
                    } else if (t == 'rejected') {
                        previewResult(file, 'Rejected');
                    } else if (t == 'bad scale') {
                        alert('Bad scale! Please restart this program and try again!')
                    } else {
                        mu = t.split(',')[0]
                        ci = t.split(',')[1]
                        previewResult(file, `${mu} +/- ${ci}`);
                    }
                });
        })
        .catch(() => { alert('Error') })
}

function previewResult(file, result) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = function() {
        let container = document.createElement('div');
        let img = document.createElement('img');
        let cap = document.createElement('p');
        container.classList.add('result-container');
        cap.textContent = result;
        img.src = reader.result;
        container.appendChild(img);
        container.appendChild(cap);
        document.getElementById('gallery').appendChild(container);
    }
}
