import os, sys, shutil, math, threading, webbrowser
from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))

import cv2
from detect import PixelSizeCalculator
from exceptions import InvalidTipCountException

UPLOAD_FOLDER = os.path.join(os.path.dirname(
    os.path.realpath(__file__)), 'upload')
ALLOWED_EXTENSIONS = {'bmp', 'png', 'jpg', 'jpeg'}
PIXEL_SIZE_CALC = PixelSizeCalculator()

app = Flask(__name__, template_folder='template', static_folder='static')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def create_pixel_size_calc_from_form(form):
    print(form)
    pixel_size_calc = PixelSizeCalculator()
    try:
        pixel_size_calc.minimum_contour_length = int(
            form.get('minimum_contour_length'))
    except:
        pass
    try:
        pixel_size_calc.minimum_ellipse_aspect_ratio = int(
            form.get('minimum_ellipse_aspect_ratio'))
    except:
        pass
    try:
        pixel_size_calc.upper_reject_ratio = float(
            form.get('upper_reject_ratio'))
    except:
        pass
    try:
        pixel_size_calc.lower_reject_ratio = float(
            form.get('lower_reject_ratio'))
    except:
        pass
    try:
        pixel_size_calc.confidence_level = float(form.get('confidence_level'))
    except:
        pass
    try:
        pixel_size_calc.max_tip_count = int(form.get('max_tip_count'))
    except:
        pass
    try:
        pixel_size_calc.min_tip_count = int(form.get('min_tip_count'))
    except:
        pass
    try:
        pixel_size_calc.canny_kernel_size = int(form.get('canny_kernel_size'))
    except:
        pass
    try:
        pixel_size_calc.distances_to_sample = int(
            form.get('distances_to_sample'))
    except:
        pass
    try:
        pixel_size_calc.canny_low_threshold = int(
            form.get('canny_low_threshold'))
    except:
        pass
    try:
        pixel_size_calc.canny_high_threshold = int(
            form.get('canny_high_threshold'))
    except:
        pass
    return pixel_size_calc


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/process', methods=['POST'])
def upload_and_process_file():
    # Check if the POST request has the file part
    if 'file' not in request.files:
        return 'no file uploaded'
    file = request.files['file']
    # If user does not select file
    if file.filename == '':
        return 'no selected file'
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        save_path = os.path.join(UPLOAD_FOLDER, filename)
        file.save(save_path)
        # Process the image
        img = cv2.imread(save_path)
        try:
            PIXEL_SIZE_CALC = create_pixel_size_calc_from_form(request.form)

            mu, ci, _ = PIXEL_SIZE_CALC.calculate_pixel_size_from_graticule(img,
                                                                            line_dist=float(request.form['scale']))
            return '{}, {}'.format(round(mu, 4), round(ci, 4))
        except InvalidTipCountException:
            return 'rejected'
        except KeyError:
            return 'bad scale'
    elif not allowed_file(file.filename):
        return 'invalid format'
    else:
        return 'invalid file'


if __name__ == '__main__':
    # Create empty directory for uploads
    if os.path.exists(UPLOAD_FOLDER):
        shutil.rmtree(UPLOAD_FOLDER)
    os.mkdir(UPLOAD_FOLDER)
    print('Initialised upload directory')
    # Open a browser in half a second (so the server has time to start)
    threading.Timer(0.5, webbrowser.open('http://localhost:5000'))
    # Start up
    app.run()
