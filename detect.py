import os
import cv2
import math
import numpy as np
from scipy.stats import t
import glob

from exceptions import InvalidTipCountException


class PixelSizeCalculator:

    def __init__(self, minimum_contour_length=40, minimum_ellipse_aspect_ratio=15,
                 upper_reject_ratio=0.3, lower_reject_ratio=0.4,
                 confidence_level=0.95, max_tip_count=150, min_tip_count=25, canny_kernel_size=3,
                 distances_to_sample=5, canny_low_threshold=100, canny_high_threshold=150):
        self.minimum_contour_length = minimum_contour_length
        self.minimum_ellipse_aspect_ratio = minimum_ellipse_aspect_ratio
        self.upper_reject_ratio = upper_reject_ratio
        self.lower_reject_ratio = lower_reject_ratio
        self.confidence_level = confidence_level
        self.max_tip_count = max_tip_count
        self.min_tip_count = min_tip_count
        self.canny_kernel_size = canny_kernel_size
        self.distances_to_sample = distances_to_sample
        self.canny_low_threshold = canny_low_threshold
        self.canny_high_threshold = canny_high_threshold

    def _distance_(self, a, b):
        x1, y1 = a
        x2, y2 = b
        return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2))

    def detect_tips(self, img, draw_dots=True):
        w, h, channels = img.shape
        # Convert to grayscale, apply histogram equalisation and canny it
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        hist = clahe.apply(gray)
        edges = cv2.Canny(hist, 50, 150, apertureSize=self.canny_kernel_size)

        # Fill in edges
        img_dilate = cv2.dilate(edges, cv2.getStructuringElement(
            cv2.MORPH_CROSS, (self.canny_kernel_size, self.canny_kernel_size)), iterations=1)
        # Flood fill and invert to make the lines chunky
        cv2.floodFill(img_dilate, np.zeros(
            (w + 2, h + 2), np.uint8), (0, 0), 255)
        fill = cv2.bitwise_not(img_dilate)
        # Blob detection
        ret, thresh = cv2.threshold(fill, 127, 255, 0)
        contours, _ = cv2.findContours(
            thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # Find tips of contours through extreme coordinates
        tips = []
        for contour in contours:
            if (len(contour) > self.minimum_contour_length):
                elps = cv2.fitEllipse(contour)
                # If major axis is more than self.minimum_ellipse_aspect_ratio times longer than minor axis
                if elps[1][1] / elps[1][0] > self.minimum_ellipse_aspect_ratio:
                    # Get rotated rectangle that fits contour
                    rect = cv2.minAreaRect(contour)
                    # Make a box out of it
                    box = cv2.boxPoints(rect)
                    # Get two pairs of points since the order in the tuple is a bit indeterminate
                    pair1 = [np.add(box[0], box[1]) / 2,
                             np.add(box[2], box[3]) / 2]
                    pair2 = [np.add(box[0], box[3]) / 2,
                             np.add(box[1], box[2]) / 2]
                    # Figure out which one reflects reality
                    if self._distance_(pair1[0], pair1[1]) > self._distance_(pair2[0], pair2[1]):
                        real_pair = pair1
                    else:
                        real_pair = pair2
                    tips.append(real_pair)
        return tips

    def get_shortest_distances(self, tips, minimum_acceptable_distance):
        # Take in list of pairs of points, calculate the shortest distance between the line defined by that pair and
        # Any other line in the list.
        distances = []
        for pair1 in tips:
            shortest_distance = 100000
            point_pairs = []

            # Get the unit direction vector of the current line
            main_dir_vec = np.subtract(pair1[1], pair1[0])
            main_dir_vec = main_dir_vec.astype(np.float32)
            main_dir_vec /= np.linalg.norm(main_dir_vec)

            # Check distance against each other point
            for pair2 in tips:
                shortest_pairs = []
                temp_distances = []

                # Get some test points on pair 2, and see the distance
                other_dir_vec = np.subtract(pair2[1], pair2[0])
                for i in range(self.distances_to_sample):
                    # Find a point on the other line between the tips
                    scale = i / (self.distances_to_sample - 1)
                    other_point = np.add(pair2[0], other_dir_vec * scale)
                    # Find the vector between the main point and the test point
                    difference = np.subtract(other_point, pair1[0])
                    # Find the projection of the test point onto the current line
                    projection = np.add(pair1[0],
                                        np.multiply(main_dir_vec, np.dot(main_dir_vec, difference)))
                    # Find the distance between this projection and the test point
                    temp_distances.append(
                        self._distance_(projection, other_point))
                    shortest_pairs.append((projection, other_point))

                # Average the distances
                current_distance = np.average(temp_distances)
                if current_distance < minimum_acceptable_distance:
                    # Skip ourselves by ignoring any distances less than a set amount of pixels pixels
                    # (we can't resolve anything with valid differences less than this many pixels w/ our canny method)
                    continue
                # Update shortest distance
                if current_distance < shortest_distance:
                    shortest_distance = round(current_distance, 3)
                    point_pairs = shortest_pairs.copy()

            distances.append((shortest_distance, point_pairs))
        return sorted(distances, key=lambda x: x[0])

    def calculate_pixel_size_from_graticule(self, img, line_dist=10, draw_dots=False, visualise=False):
        w, h, _ = img.shape
        tips = self.detect_tips(img, draw_dots)
        if draw_dots:
            for pair in tips:
                cv2.circle(img, tuple(pair[0].astype(
                    np.int32)), 3, (0, 255, 255), -1)
                cv2.circle(img, tuple(pair[1].astype(
                    np.int32)), 3, (0, 255, 0), -1)
        # Check if there are too few or too many tips - indicative of a poor image
        if len(tips) > self.max_tip_count or len(tips) < self.min_tip_count:
            raise InvalidTipCountException()
        dists = self.get_shortest_distances(
            tips, 0.0022 * np.sqrt(img.shape[0] * img.shape[1]))
        # Reject bottom self.lower_reject_ratio % of distances, likewise for the top end
        dists = dists[round(len(dists) * self.lower_reject_ratio):round(len(dists) * (1 - self.upper_reject_ratio))]
        # Calculate ratio of pixel to micron
        ratios = list(map(lambda x: line_dist / x[0], dists))
        mu = np.mean(ratios)
        # Calculate bias-corrected std
        sigma = np.std(ratios, ddof=1)
        # Calculate T for 2-tailed 95% CI
        cv = t.ppf(((1 + self.confidence_level) / 2), len(ratios) - 1)
        # Visualise all distances not rejected.
        if visualise:
            # colorarray = [random.randint(100,255),random.randint(100,255),random.randint(100,255)]
            colorarray = [250, 120, 200]
            for pairs in dists:
                pairs = list(map(lambda pair: tuple(map(lambda x: tuple(map(round, x)), pair)), pairs[1]))
                for i in range(len(colorarray)):
                    colorarray[i] %= 255
                for i in range(self.distances_to_sample):
                    cv2.circle(img, tuple(pairs[i][0]), 1, tuple(colorarray), thickness=-1)
                    cv2.circle(img, tuple(pairs[i][1]), 1, tuple(colorarray), thickness=-1)
                    cv2.line(img, tuple(pairs[i][0]), tuple(pairs[i][1]), tuple(colorarray))

        return mu, cv * sigma, img


if __name__ == '__main__':
    # Init PixelSizeCalculator
    pixel_size_calculator = PixelSizeCalculator()
    # Run on some images
    img_paths = glob.glob('Images/*.*')
    dists = []
    for img_path in img_paths:
        # Load image
        img = cv2.imread(img_path)
        # Print detection
        text = '{} - '.format(os.path.basename(img_path))
        try:
            mu, ci, img = pixel_size_calculator.calculate_pixel_size_from_graticule(
                img, draw_dots=True)
            text += '1 pixel = {} +/- {} um ({:.0%} CI)'.format(
                np.round(mu, 3),
                np.round(ci, 3),
                0.95
            )
        except InvalidTipCountException:
            text += 'Rejected'
        print(text)
        cv2.putText(img, text, (img.shape[1] // 16, img.shape[0] // 8),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 4)
        # Uncomment to show image
        # cv2.imshow('image', img)
        # cv2.waitKey()
        newpath = os.path.join(r'images/Done', os.path.basename(img_path))
        # print(newpath)
        cv2.imwrite(newpath, img)
