# Automated Microscope Scale Calibration

This is a tool for automatically calculating the pixel-scale of a microscope slide using a scale micrometer. This tool works for both cross-type micrometers and line micrometers. Example images of the kinds of microscopes supported can be found in the `images/` folder. 

## Installation

To use this tool, you will need Python 3. The tool can be installed by cloning the repository and installing all dependencies in `requirements.txt`. The latter can be done with the command `pip install -r requirements.txt`.

## Usage

To start the tool, navigate to the repository's root directory and run the command `python frontend/gui.py`. 

Results are outputted in microns (μm) ± the 95% confidence interval.

Default settings are selected to work sensibly with the cameras, microscope and micrometers which we tested them on, however a full customisation of settings is also possible through the GUI.

A video explaining usage of the program can be found in `demo.mp4`.

## Method

The tool works by identifying the tips of gradations using standard image processing techniques. Distances between nearby pairs of tips are found and from this a scale is calculated for the image. It is relatively robust to noise, as can be seen in `images/8_with_extreme_distractions.png` whose 95% confidence interval contained the manually calculated ground truth value. Further testing on other types of smartphones, microscopes and micrometers would be an avenue for improving the tool.

## Project Structure

`/` - Python files responsible for processing and a demo video.

`/images/` - Contains folders `250x/` and `100x/` which contain images used to design the tool.

`/frontend/` - Contains the GUI program. `gui.py` will start a Flask server and open a window in your browser to use the tool on `localhost:5000` from which point the instructions in `demo.mp4` can be followed.

## Special Parameters

`Minimum contour length` - Sets the distance in pixels below which any lines will be rejected (this should be set to at least half of the length of the gradations in pixels). **Default 25**

`Minimum ellipse aspect ratio` - Sets the minimum aspect ratio, i.e the ratio of width to height of the gradations below which they are rejected by the detection algorithm. This factor depends on micrometer, but for our micrometers an aspect ratio of 20 was appropriate. **Default 20**

`Upper reject ratio` - The fraction of the distance pairs that are rejected from the top end of the measured distances. Should increase as image quality decreases. **Default 0.3**

`Lower reject ratio` - Same as above, but for the lower end of the distances. **Default 0.4**

`Confidence level` - The size of the confidence interval that the algorithm calculates. **Default 0.95**

`Max tip count` - If more gradations than this number are detected the image is rejected. Should be higher than the amount of gradations in your image. **Default 150**

`Min tip count` - Same as above, but rejects when not enough tips are detected. **Default 25**

`Canny kernel size` - Determines the size of the canny edge detection kernel. Not recommended to change from default. **Default 3**

`Distances to sample` - Determines the amount of distances between each pair of points to sample to calculate the pixel size of the image. Increasing this too much will artificially narrow the confidence interval. **Default 5**

`Canny low threshold` - Determines the low threshold for the canny edge detector. If results are insufficient on images with aberration or blurry/non-sharp edges of gradations this is recommended. The default is already relatively on the high end. **Default 100**

`Canny high threshold` - Determines the high threshold for the canny edge detector. If your image's micrometers look greyish or not particularly strong black then decreasing first this and then maybe the canny low threshold is advisable. There must be a decent gap between these two thresholds. **Default 150**
